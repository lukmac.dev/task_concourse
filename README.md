# Task_Concourse


## Setup


### SETUP CONCOURSE

https://concourse-ci.org/install.html

    wget https://concourse-ci.org/docker-compose.yml
    docker-compose up -d
    // Beenden 
    docker-compose down
    docker ps


### SETUP FLY FILE

FLI is a CLI Tool
Wird immer durch <./fly> aufgerufen.
Hilfestellungen: ./fly -help
./fly targets

    chmod u+x fly


### START CONCOURSE
    cd <root directory)
    ./fly -t fh login -u test -p test --concourse-url http://127.0.0.1:8080

Die Applikation kann im Browser unter http://localhost:8080/ erreicht werden.


### Browser Login
Username: test
Password: test


### Set Pipeline

    ./fly -t fh set-pipeline -c 'ci/pipeline.yml' -p pipeline

http://127.0.0.1:8080/login?fly_port=53440   // FAILED HERE

http://127.0.0.1:8080/teams/main/pipelines/pipeline/jobs/commit-stage